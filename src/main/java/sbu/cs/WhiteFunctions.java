package sbu.cs;

public class WhiteFunctions {

    public static String getValue(int status, String input1, String input2)
    {
        String output ="";

        switch(status)
        {
            case 1 :
                output = function_1(input1, input2);
                break;

            case 2 :
                output = function_2(input1, input2);
                break;

            case 3 :
                output = function_3(input1, input2);
                break;

            case 4 :
                output = function_4(input1, input2);
                break;

            case 5 :
                output = function_5(input1, input2);
                break;
        }

        return output;
    }

    private static String function_1(String input1, String input2)
    {
        StringBuilder output = new StringBuilder();

        int index = Math.min(input1.length(), input2.length());

        for(int i = 0; i < index; i++)
        {
            output.append(input1.charAt(i));
            output.append(input2.charAt(i));
        }

        output.append(input1.substring(index));
        output.append(input2.substring(index));

        return output.toString();
    }

    private static String function_2(String input1, String input2)
    {
        return input1 + new StringBuilder().append(input2).reverse().toString();
    }

    private static String function_3(String input1, String input2)
    {
        return function_1(input1, new StringBuilder(input2).reverse().toString());
    }

    private static String function_4(String input1, String input2)
    {
        if(input1.length() % 2 == 0)
            return input1;
        else
            return input2;
    }

    private static String function_5(String input1, String input2)
    {
        StringBuilder output = new StringBuilder();

        int index = Math.min(input1.length(), input2.length());

        for(int i = 0; i < index; i++)
        {
            char c = (char)(((input1.charAt(i) - 'a' + input2.charAt(i) - 'a') % 26) + 'a');
            output.append(c);
        }

        output.append(input1.substring(index));
        output.append(input2.substring(index));

        return output.toString();
    }


}
