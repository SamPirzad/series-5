package sbu.cs;

public class ColorChecker {

    public static boolean isGreen(int n, int j, int i){
        if((i == 0 && j < n - 1) || (j == 0 && i < n - 1)){
            return true;
        }
        return false;
    }

    public static boolean isBlue(int n, int j, int i){
        if((i > 0 && i < n - 1) && (j > 0 && j < n - 1)){
            return true;
        }
        return false;
    }

    public static boolean isYellow(int n, int j, int i){
        if((i == 0 && j == n - 1) || (j == 0 && i == n - 1)){
            return true;
        }
        return false;
    }

    public static boolean isPink(int n, int j, int i){
        if((i > 0 && j == n - 1) || (j > 0 && i == n - 1)){
            return true;
        }
        return false;
    }


}
