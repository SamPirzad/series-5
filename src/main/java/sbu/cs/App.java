package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        //return null;
        String[][] Arr = new String[n][n];

        for (int j = 0; j < n; j++) {
            for(int i = 0; i < n; i++){

                if(ColorChecker.isGreen(n, j, i))
                    Arr = Green.getValue(Arr, input, arr, j, i);

                else if(ColorChecker.isYellow(n, j, i))
                    Arr = Yellow.getValue(Arr, arr, j, i);

                else if(ColorChecker.isBlue(n, j, i))
                    Arr = Blue.getValue(Arr, arr, j, i);

                else if(ColorChecker.isPink(n, j, i))
                    Arr = Pink.getValue(Arr, arr, j, i);
            }
        }
        return Arr[n - 1][n - 1];
    }
}
