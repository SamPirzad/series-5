package sbu.cs;

public class Pink {

    public static String[][] getValue(String[][] strArr, int[][] arr, int j, int i){


        if(i == arr.length - 1) {
            strArr[j][i] = WhiteFunctions.getValue(arr[j][i] , strArr[j][i - 1].split(Blue.regex)[0] , strArr[j - 1][i]);
        }else {
            strArr[j][i] = WhiteFunctions.getValue(arr[j][i] , strArr[j][i - 1] , strArr[j - 1][i].split(Blue.regex)[1]);
        }
        return strArr;
    }
}
