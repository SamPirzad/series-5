package sbu.cs;

public class SortArray {

    private void swap(int[] array,int i,int j)
    {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        //return null;

        for(int i = 0; i < size; i++)
        {
            int min = i;

            for(int j = i + 1; j < size; j++)
            {
                if(arr[min] > arr[j])
                    min = j;
            }
            swap(arr,min,i);
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        //return null;

        for(int i = 1; i < size; ++i)
        {
            int j = i;

            while(j > 0 && arr[j-1] > arr[j])
            {
                swap(arr,j,j-1);
                j--;
            }
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        //return null;
        if (size < 2)
            return arr;

        int avg = size / 2;
        int[] temp = new int[avg];
        int[] delta = new int[size - avg];

        for (int i = 0; i < avg; i++)
            temp[i] = arr[i];

        for (int i = avg; i < size; i++)
            delta[i - avg] = arr[i];

        mergeSort(temp, avg);
        mergeSort(delta, size - avg);
        merge(arr, temp, delta, avg, size - avg);

        return arr;
    }

    public static void merge(int[] arr, int[] temp, int[] delta, int start, int end) {

        int i = 0, j = 0, k = 0;
        while (i < start && j < end) {
            if (temp[i] <= delta[j])
                arr[k++] = temp[i++];
            else
                arr[k++] = delta[j++];
        }
        while (i < start) {
            arr[k++] = temp[i++];
        }
        while (j < end) {
            arr[k++] = delta[j++];
        }
    }



    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        //return -1;

        int start = 0;
        int end = arr.length - 1;
        int avg = (start + end) / 2;

        while(start <= end)
        {
            if(arr[avg] == value)
                return avg;

            else if(arr[avg] > value)
                end = avg - 1;

            else
                start = avg + 1;
        }
        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        //return -1;

        return BSR_Operator(arr, 0, arr.length, value);
    }

    private int BSR_Operator(int[] arr,int start,int end,int value)
    {
        if (start <= end)
        {
            int middle = start + (end - start) / 2;
            if (arr[middle] == value) {
                return middle;
            }
            else if (arr[middle] > value) {
                return BSR_Operator(arr, start, middle - 1, value);
            }
            else {
                return BSR_Operator(arr, middle + 1, end, value);
            }
        }
        return -1;
    }

}


