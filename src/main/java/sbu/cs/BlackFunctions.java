package sbu.cs;

public class BlackFunctions {

    public static String getValue(int status, String input)
    {
        String output = "";

        switch(status)
        {
            case 1 :
                output = function_1(input);
                break;

            case 2 :
                output = function_2(input);
                break;

            case 3 :
                output = function_3(input);
                break;

            case 4 :
                output = function_4(input);
                break;

            case 5 :
                output = function_5(input);
                break;
        }

        return output;
    }

    private static String function_1(String input)
    {
        return new StringBuilder(input).reverse().toString();
    }

    private static String function_2(String input)
    {
        StringBuilder output = new StringBuilder();

        for(int i = 0; i < input.length(); i++)
        {
            output.append(input.charAt(i));
            output.append(input.charAt(i));
        }

        return output.toString();
    }

    private static String function_3(String input)
    {
        StringBuilder output = new StringBuilder(input);
        output.append(input);

        return output.toString();
    }

    private static String function_4(String input)
    {
        return input.charAt(input.length() - 1)
                + input.substring(0, input.length() - 1);
    }

    private static String function_5(String input)
    {
        StringBuilder output = new StringBuilder();

        for(int i = 0; i < input.length(); i++)
            output.append((char)('z' - (input.charAt(i) - 'a')));

        return output.toString();
    }
}
