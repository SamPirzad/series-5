package sbu.cs;

public class Green {

    public static String[][] getValue(String[][] strArr, String input, int[][] arr, int j, int i){

        if (i == 0 && j == 0){
            strArr[j][i] = BlackFunctions.getValue(arr[j][i], input);
        } else{
            if (i == 0){
                strArr[j][i] = BlackFunctions.getValue(arr[j][i], strArr[j - 1][i]);
            } else{
                strArr[j][i] = BlackFunctions.getValue(arr[j][i], strArr[j][i - 1]);
            }
        }
        return strArr;
    }
}
